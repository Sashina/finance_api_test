<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserController@api_login')->name('login');
Route::post('register', 'UserController@api_register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'UserController@api_details');
    Route::get('user', 'UserController@index');
    Route::get('logout', 'UserController@api_logout');
});

//------------------ Account --------------------------//
Route::middleware('auth:api')->group( function () {
    Route::resource('accounts', 'AccountController');
    Route::get('account/search/{keywords}', 'AccountController@search');
    Route::get('account/restore/{account}', 'AccountController@restore');

//--------------- Transaction ------------------------------//
    Route::get('transaction','TransactionController@index')->name('transaction');
    Route::post('transaction', 'TransactionController@save');
    Route::put('transaction/{transactions}', 'TransactionController@update');
    Route::delete('transaction/{transactions}', 'TransactionController@delete');
    Route::get('transaction/restore/{transactions}', 'TransactionController@restore');
    Route::get('transaction/restore_all', 'TransactionController@restoreAll');

//--------------- Report ------------------------------//
    //Route::post('report-transaction-by-account','ReportController@reporttransactionbyaccount');
    Route::get('transaction-summary','ReportController@reportallbalance');
    Route::post('transaction-summary','ReportController@reporttransactionbyaccount');
    Route::get('report-transaction','ReportController@reportbalance');
    Route::post('report-transaction-by-account','ReportController@reportbalancebyaccount');

    Route::post('report-transaction-account-by-month','ReportController@transactionaccountbymonth');
    Route::post('report-transaction-account-by-date','ReportController@transactionaccountbyday');
});

