<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTransactionView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('v_transaction');
        DB::statement('
            CREATE VIEW v_transaction AS
            SELECT
            accounts.id,accounts.account_name,transactions.transaction_flag, transactions.transaction_name,transactions.transaction_date,
                case when transaction_flag = "i" then transaction_amount end as cash_in,
                case when transaction_flag = "o" then transaction_amount end as cash_out
            FROM
                transactions 
            LEFT JOIN accounts ON transactions.account_id = accounts.id;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_transaction');
    }
}
