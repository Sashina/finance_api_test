<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE PROCEDURE procedure_saldo()
            BEGIN
            SELECT accounts.id,accounts.account_name,transactions.transaction_name,
                    @d:=if(transaction_flag="i",transaction_amount,0) as debet,
                    @k:=if(transaction_flag="o",transaction_amount,0) as Kredit,
                    @saldo:=@saldo+@d-@k AS saldo 
            FROM transactions JOIN (SELECT @saldo:=0) a 
            LEFT JOIN accounts ON transactions.account_id = accounts.id  
            ORDER BY transactions.created_at asc ,accounts.account_name ASC;
            END'
        );

        DB::unprepared('
            CREATE PROCEDURE procedure_saldo_by_id(IN idx int)
            BEGIN
            SELECT accounts.id,accounts.account_name,transactions.transaction_name,
                    @d:=if(transaction_flag="i",transaction_amount,0) as debet,
                    @k:=if(transaction_flag="o",transaction_amount,0) as Kredit,
                    @saldo:=@saldo+@d-@k AS saldo 
            FROM transactions JOIN (SELECT @saldo:=0) a 
            LEFT JOIN accounts ON transactions.account_id = accounts.id  
            WHERE accounts.id = idx
            ORDER BY transactions.created_at asc ,accounts.account_name ASC;
            END'
        );
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS procedure_saldo');
        DB::unprepared('DROP PROCEDURE IF EXISTS procedure_saldo_by_id');
    }
}
