<?php

use Illuminate\Database\Seeder;
use App\Accounts;
class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Accounts::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 100; $i++) {
            Accounts::create([
                'account_name' => $faker->name,
                'account_phone' => $faker->PhoneNumber,
                'account_email' => $faker->unique()->safeEmail,
                'account_address' => $faker->address
            ]);
        }
    }
}
