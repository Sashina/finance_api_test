The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

<h1>API Finance</h1>

README

Please read the following instructions before launching the development server.

This repository contains the codes of API Finance development. How to start local development:
- Pull the latest version available
- Setup the database, create db name as you like.
- Edit the .env file to match with db name and credentials.
- php artisan migrate:refresh --seed
- php artisan passport:install --force
- php artisan serve
- If some dependencies are outdated: composer update
- This API testing using Postman
- In Postman, API using Header Accept : application/json and Authorization : Bearer [Your token]
- **Please Be Careful with your Login Token, Please Change your Authorization when you relogin.**
- Documentation Link for API https://www.getpostman.com/collections/0e43e7ef519d93340057






