<?php

namespace App;

use App\Filters\TransactionFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transactions extends Model
{
    use SoftDeletes;
    
    protected $table = "transactions";

    protected $dates = ['deleted_at'];
       
    protected $fillable = ['account_id', 'transaction_name', 'transaction_date', 'transaction_flag','transaction_amount'];


    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }

    public function scopeFilter(Builder $builder, $request)
    {
        return (new TransactionFilter($request))->filter($builder);
    }
}
