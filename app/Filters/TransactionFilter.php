<?php

namespace App\Filters;

use App\Filters\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class TransactionFilter extends AbstractFilter
{
    protected $filters = [
        'transaction_flag' => TypeFilter::class
    ];
}