<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    protected $table = "saldos";

    protected $dates = ['deleted_at'];
       
    protected $fillable = ['account_id', 'saldo_amount'];



    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
}
