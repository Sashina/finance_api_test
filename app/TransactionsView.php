<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionsView extends Model
{
    protected $table = "transaction_records";
}
