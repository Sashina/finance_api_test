<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateOrReplaceTransactionRecordsViewCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:CreateOrReplaceTransactionRecordsView';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create or Replace SQL View.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('
            CREATE OR REPLACE VIEW transaction_records AS
            SELECT
                accounts.account_name, transactions.transaction_name,transactions.transaction_date,
                case when transaction_flag = "i" then transaction_amount end as cash_in,
                case when transaction_flag = "o" then transaction_amount end as cash_out
            FROM
                transactions 
            LEFT JOIN accounts ON transactions.account_id = accounts.id;
        ');
    }
}
