<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseController as ResponseController;
use App\Accounts;
use App\Saldo;
use App\Transactions;
use App\TransactionsView;
use DB;

class ReportController extends ResponseController
{
    public $successStatus = 200;

    public function reporttransactionbyaccount(Request $request){
        $report = Transactions::where('account_id',$request->account_id)->get();
        return $this->sendResponse($report->toArray(), 'Report retrieved successfully.');
    }

    public function reportalltransaction(){
        $report = TransactionsView::paginate(10);
        return $this->sendResponse($report->toArray(), 'Report retrieved successfully.');
    }

    public function reportallbalance(){
        $report = Saldo::paginate(10);
        return $this->sendResponse($report->toArray(), 'Report retrieved successfully.');
    }

    public function reportbalance(){
        $transaction = DB::select('call procedure_saldo()');
        return $this->sendResponse($transaction, 'Data retrieved successfully.');
    }

    public function reportbalancebyaccount(Request $request){
        $transaction = DB::select('call procedure_saldo_by_id('.$request->id.')');
        return $this->sendResponse($transaction, 'Data retrieved successfully.');
    }

    public function transactionaccountbymonth(Request $request){
        $varField = ($request->type == 'i')?'cash_in':'cash_out';
           
        $cash = TransactionsView::where('id',$request->account_id)
                        ->where('transaction_flag',$request->type)
                        ->whereMonth('transaction_date','=',$request->month)
                        ->get();
        
        $summary = TransactionsView::select(DB::raw('id,account_name,sum('.$varField.') AS total_transaction'))
                        ->where('id',$request->account_id)
                        ->where('transaction_flag',$request->type)
                        ->whereMonth('transaction_date','=',$request->month)
                        ->groupBy('id','account_name')
                        ->get();
        
        $data['cash'] =  $cash;                
   
        $data['summary'] =  $summary;
        
        return response()->json($data, 200); 
    }

    public function transactionaccountbyday(Request $request){ 
        $varField = ($request->type == 'i')?'cash_in':'cash_out';
           
        $cash = TransactionsView::where('id',$request->account_id)
                    ->where('transaction_flag',$request->type)
                    ->whereDate('transaction_date',$request->transaction_date)
                    ->get();

        $summary = TransactionsView::select(DB::raw('id,account_name,sum('.$varField.') AS total_transaction'))
                    ->where('id',$request->account_id)
                    ->where('transaction_flag',$request->type)
                    ->whereDate('transaction_date',$request->transaction_date)
                    ->groupBy('id','account_name')
                    ->get();

        $data['cash'] =  $cash;                
        $data['summary'] =  $summary;

        return response()->json($data, 200); 
    }
}
