<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseController as ResponseController;
use App\Accounts;
use App\Saldo;
use App\Transactions;
use DB;

class TransactionController extends ResponseController
{
   public function save(Request $request)
   {
       //VALIDASI
       $this->validate($request, [
           'account_id' => 'required',
           'transaction_name' => 'required',
           'transaction_date' => 'required',
           'transaction_flag' => 'required',
           'transaction_amount' => 'required | numeric'
       ]);

       DB::beginTransaction();

       try {
           $account = $request->account_id;
           
           $balance = $this->saldo($request->account_id, $request->transaction_flag, $request->transaction_amount, 'insert',null);
           if($balance == true){
                $transaction = Transactions::create($request->all());
                DB::commit();
                return $this->sendResponse($transaction, 'Transaction successfully inserted.');
           }else{
                DB::rollback();
                return $this->sendError('Transaction error', 404);
           }
           
           
       } catch(\Exception $e) {
            DB::rollback();
            return $this->sendError('Transaction error', 404);
       }
   }

   public function update(Request $request, $transactions)
   {
       //VALIDASI
       $this->validate($request, [
           'account_id' => 'required',
           'transaction_name' => 'required',
           'transaction_date' => 'required',
            'transaction_flag' => 'required',
           'transaction_amount' => 'required | numeric'
       ]);

        try {
            
            $transaction = Transactions::findOrFail($transactions);
            
            if($request->transaction_flag != $transaction->transaction_flag){
                return $this->sendError('Sorry, The transaction type can not be changed', 404);
            }

            $balance = $this->saldo($request->account_id,$transaction->transaction_flag, $request->transaction_amount,'update',$transaction->id);
            
            if($balance == true){
                $transaction = $transaction->update($request->all());
            
                return $this->sendResponse($transaction, 'Transaction successfully updated.');
            }else{
                return $this->sendError('Transaction error', 404);
            }
      
        } catch(\Exception $e) {
            return $this->sendError('Transaction error', 404);
       }
   }

   public function delete(Request $request, $transactions)
   {
        $transaction = Transactions::findOrFail($transactions);

        $balance = $this->saldo($transaction->account_id,$transaction->transaction_flag, $transaction->transaction_amount,'delete',$transaction->id);

        $transaction->delete();

        $success['Message'] = 'Data deleted';

       return response()->json( $success, 204);
   }

   public function restore(Request $request, $transactions)
   {
       $transaction = Transactions::withTrashed()->find($transactions)->restore();
      
       $transaction = Transactions::where('id', $transactions)->first();
       
       $balance = $this->saldo($transaction->account_id,$transaction->transaction_flag, $transaction->transaction_amount,'restore',$transaction->id);

       
       return response()->json($transaction, 200);
   }

    public function saldo($account = null, $type = null, $amount = null, $action = null, $id=null){
        $saldo = Saldo::select('saldo_amount')->where('account_id', $account)->first();
        $transaction = Transactions::select('transaction_amount','transaction_flag')->where('id', $id)->first();
        $saldo = ($saldo != null) ? $saldo->saldo_amount : 0;

        if($action=='insert'){
            if($type == 'i'){
                $ending_balance = $saldo + $amount;
            }elseif($type == 'o'){
                $ending_balance = $saldo - $amount;
            }else{
                return $this->sendError('Transaction type does not match, please choose i or o', 404); 
            }
        //------------------ update saldo ----------------------------------
        }elseif($action=='update'){
            
            if($type == 'i'){
                if($amount < $transaction->transaction_amount){
                    $kurang = $transaction->transaction_amount - $amount;
                    $ending_balance = $saldo - $kurang;
                }elseif($amount > $transaction->transaction_amount){
                    $lebih = $amount- $transaction->transaction_amount;
                    $ending_balance = $saldo + $lebih;
                }else{
                    $ending_balance = $saldo;
                }
                
            }elseif($type == 'o'){
                if($amount < $transaction->transaction_amount){
                    $kurang = $transaction->transaction_amount - $amount;
                    $ending_balance = $saldo + $kurang;
                }elseif($amount > $transaction->transaction_amount){
                    $lebih = $amount- $transaction->transaction_amount;
                    $ending_balance = $saldo - $lebih;
                }else{
                    $ending_balance = $saldo;
                } 
            }
        //------------------ delete saldo ------------------------------
        }elseif($action == 'delete'){
            if($type == 'i'){
                $ending_balance = $saldo - $transaction->transaction_amount;
            }else{
                $ending_balance = $saldo + $transaction->transaction_amount;
            }
        //------------------ restore saldo ------------------------------
        }elseif($action == 'restore'){
            if($type == 'i'){
                $ending_balance = $saldo + $transaction->transaction_amount;
            }else{
                $ending_balance = $saldo - $transaction->transaction_amount;
            }
        }else{
            return $this->sendError('Action Not Found', 404);
        }

        try{
            if($saldo != null){
                $update = Saldo::where('account_id', $account)
                                ->update(['saldo_amount' => $ending_balance]);
            }else{
                Saldo::insert(
                    ['account_id' => $account, 'saldo_amount' => $ending_balance]
                );
            }
            return true;
        }catch(\Exception $e){
            return false;
        }
    }
}
