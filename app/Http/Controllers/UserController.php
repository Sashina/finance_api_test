<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseController as ResponseController;
use App\User;
use Validator;

class UserController extends ResponseController
{
    public $successStatus = 200;

    public function index()
    {
        $user = User::paginate(10);
        return $this->sendResponse($user->toArray(), 'User retrieved successfully.');
    }

    public function api_register(Request $request) 
    { 
        //validation
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('financeToken')-> accessToken; 
        $success['user_detail'] =  $user;
        return response()->json(['success'=>$success], $this-> successStatus); 
    }

    public function api_login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('financeToken')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function api_details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    public function api_logout(Request $request) 
    { 
        if (Auth::user()) {
            $user = Auth::user()->token();
            $user->revoke();
    
            return response()->json([
              'success' => true,
              'message' => 'Logout success'
            ]);
        }else {
            return response()->json([
              'success' => false,
              'message' => 'Unable to Logout'
            ]);
        }
        
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus, 200); 
    }
}
