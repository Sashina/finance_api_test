<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseController as ResponseController;
use App\Accounts;
use Validator;

class AccountController extends ResponseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        $account = Accounts::paginate(10);
        return $this->sendResponse($account->toArray(), 'Account retrieved successfully.');
    }

    public function search($keywords)
    {
        try{
            $data = Accounts::where(function ($query) use ($keywords) {
                $query->where('account_name', 'like', '%'.$keywords.'%')
                    ->orWhere('account_phone', 'like', '%'.$keywords.'%')
                    ->orWhere('account_email', 'like', '%'.$keywords.'%')
                    ->orWhere('account_address', 'like', '%'.$keywords.'%');
            })->paginate();

            if (!$data->isEmpty()) {
                return $this->sendResponse($data->toArray(), 'Data Found.');
            }else{
                return $this->sendError('Data not found.');
            }
        } catch(\Exception $e) {
            return $this->sendError('Data not found', 404);
        }
        
    }

    public function show($account)
    {
        $account = Accounts::find($account);
        if (is_null($account)) {
            return $this->sendError('Account not found.');
        }
        // return response()->json(['data' => $account]);
        return $this->sendResponse($account->toArray(), 'Account retrieved successfully.');

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'account_name' => 'required', 
            'account_phone' => 'required|numeric|digits:12 ', 
            'account_email' => 'required|email|unique:accounts', 
            'account_address' => 'required', 
        ]);
        
        if ($validator->fails()) { 
            return $this->sendError('Validation Error.', $validator->errors());   
        }
        
        try{
            $accounts = Accounts::create($request->all());
            return $this->sendResponse($accounts->toArray(), 'Account saved successfully.');
        } catch(\Exception $e) {
            return $this->sendError('Save data failed', 404);
        }
    }

    public function update(Request $request, $account)
    {
        //asumsi email tidak dapat diubah
       
        $validator = Validator::make($request->all(), [ 
            'account_name'  => 'required', 
            'account_phone' => 'required|numeric|digits:12 ',  
            'account_address' => 'required', 
        ]);
        
        if ($validator->fails()) { 
            return $this->sendError('Validation Error.', $validator->errors());   
        }

        try{
            $accounts = Accounts::findOrFail($account);
            
            $updated = $accounts->update($request->all());
            
            return $this->sendResponse($accounts->toArray(), 'Account updated successfully.');
        } catch(\Exception $e) {
            return $this->sendError('Update data failed', 404);
        }
    }

    public function destroy(Accounts $account)
    {
        try{
            $account->delete();
            return $this->sendResponse($account->toArray(), 'Account deleted successfully.');
        } catch(\Exception $e) {
            return $this->sendError('Delete data failed', 404);
        }
    }

    public function restore(Request $request, $account)
    {
        try{
            Accounts::withTrashed()->find($account)->restore();
            
            return $this->sendResponse('', 'Account restored successfully.');

        } catch(\Exception $e) {
            return $this->sendError('Restore data failed', 404);
        }
    }


}
