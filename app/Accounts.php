<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accounts extends Model
{
    use SoftDeletes;

    protected $table = "accounts";

    protected $dates = ['deleted_at'];
       
    protected $fillable = ['account_name', 'account_phone', 'account_email', 'account_address'];

    public function financial()
    {
        return $this->hasMany(Transactions::class);
    }

    public function saldo()
    {
        return $this->hasOne(Saldo::class);
    }


}
